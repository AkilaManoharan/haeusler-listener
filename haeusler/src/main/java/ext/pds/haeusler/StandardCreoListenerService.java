package ext.pds.haeusler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wt.epm.workspaces.EPMWorkspaceManagerEvent;
import wt.fc.PersistenceManagerEvent;
import wt.services.ManagerException;
import wt.services.StandardManager;
import wt.util.WTException;
import wt.vc.VersionControlServiceEvent;
import wt.vc.wip.WorkInProgressServiceEvent;

/**
 *
 * @author akila
 */
public class StandardCreoListenerService extends StandardManager implements CreoListenerService {

    private static final Logger logger = LoggerFactory.getLogger(StandardCreoListenerService.class);

    public static StandardCreoListenerService newStandardCreoListenerService() throws WTException {
        StandardCreoListenerService instance = new StandardCreoListenerService();
        instance.initialize();
        logger.trace("StandardCreoListenerService started");
        return instance;
    }

    @Override
    protected void performStartupProcess() throws ManagerException {
        CreoEventListener listener = new CreoEventListener();
        getManagerService().addEventListener(listener,PersistenceManagerEvent.generateEventKey(PersistenceManagerEvent.POST_STORE));
        getManagerService().addEventListener(listener, VersionControlServiceEvent.generateEventKey(VersionControlServiceEvent.NEW_VERSION));
        getManagerService().addEventListener(listener, EPMWorkspaceManagerEvent.generateEventKey(EPMWorkspaceManagerEvent.PRE_WORKSPACE_CHECKIN));
        getManagerService().addEventListener(listener, EPMWorkspaceManagerEvent.generateEventKey(EPMWorkspaceManagerEvent.POST_WORKSPACE_CHECKIN));
        getManagerService().addEventListener(listener, EPMWorkspaceManagerEvent.generateEventKey(EPMWorkspaceManagerEvent.NEW_TO_WORKSPACE));
        getManagerService().addEventListener(listener, EPMWorkspaceManagerEvent.generateEventKey(EPMWorkspaceManagerEvent.CHECKOUT_TO_WORKSPACE));
        getManagerService().addEventListener(listener, WorkInProgressServiceEvent.generateEventKey(WorkInProgressServiceEvent.PRE_CHECKIN));
        getManagerService().addEventListener(listener, WorkInProgressServiceEvent.generateEventKey(WorkInProgressServiceEvent.POST_CHECKIN));
    }


}
