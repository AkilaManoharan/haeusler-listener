package ext.pds.haeusler;

import ext.pdmlink.log.LogHelper;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wt.doc.WTDocument;
import wt.epm.EPMDocument;
import wt.events.KeyedEvent;
import wt.fc.collections.WTCollection;
import wt.part.WTPart;
import wt.services.ServiceEventListenerAdapter;
import wt.type.Typed;
import wt.util.WTException;
import wt.util.WTPropertyVetoException;


/**
 *
 * @author akila
 */
public class CreoEventListener  extends ServiceEventListenerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(CreoEventListener.class);

    public CreoEventListener() {
        super(CreoListenerService.class.getName());
    }

    @Override
    public void notifyVetoableMultiObjectEvent(Object event) throws WTException, WTPropertyVetoException, RemoteException {
        if (logger.isTraceEnabled()) {
            logger.trace("notifyVetoableMultiObjectEvent(Object event)");
            logger.trace("event: {}", ((KeyedEvent) event).getEventKey());
        }

        WTCollection targets = (WTCollection) ((KeyedEvent) event).getEventTarget();
        Iterator itr = targets.persistableIterator();

        while (itr.hasNext()) {
            Object next = itr.next();
            logger.trace("Object {} ,Event {} ",LogHelper.toString(next), ((KeyedEvent) event).getEventKey());
        }

    }

 
}
