package ext.pds.haeusler;

import ext.pdmlink.service.StartupUtil;
import ext.pdsvision.annotations.Service;
import wt.method.RemoteInterface;

/**
 *
 * @author akila
 */
@RemoteInterface
@Service (implementedBy = StandardCreoListenerService.class, id="98029", context=StartupUtil.METHOD_SERVER)
public interface CreoListenerService {
    
}
